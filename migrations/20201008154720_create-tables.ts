import * as Knex from 'knex';

export async function up(knex: Knex): Promise<void> {
  if (await knex.schema.hasTable('visitor')) {
    return;
  }
  await knex.schema.createTable('visitor', table => {
    table.integer('count');
  });
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTableIfExists('visitor');
}
