import { startServer, stopServer } from '../server';
import { Server } from 'http';
import * as fetch from 'node-fetch';

describe('Server TestSuit', () => {
  let server: Server;
  let port: number = 8100;

  beforeAll(async () => {
    server = await startServer(port);
  });

  afterAll(async () => {
    await stopServer(server);
  });

  test.skip('should get home page', async () => {
    let res = await fetch(`http://localhost:${port}`);
    expect(res.status).toBe(200);
  });

  test('should count visitor per request to webpages', async () => {
    let firstRes = await fetch(`http://localhost:${port}/count`);
    let firstCount = (await firstRes.json()).count;

    let res= await fetch(`http://localhost:${port}`);
    expect(res.status).toBe(200)

    await new Promise((resolve)=>{
        setTimeout(resolve, 1000)
    })

    let secondRes = await fetch(`http://localhost:${port}/count`);
    let secondCount = (await secondRes.json()).count;

    expect(secondCount).toBe(firstCount + 1);
  });
});
