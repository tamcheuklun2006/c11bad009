import * as Knex from "knex";

export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries
    await knex("visitor").del();

    // Inserts seed entries
    await knex("visitor").insert([
        { count: 0 },
    ]);
};
